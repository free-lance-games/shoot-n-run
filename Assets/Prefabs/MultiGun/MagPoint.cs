using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagPoint : MonoBehaviour
{
    // Start is called before the first frame update
    public MultiGun Handler;
    public int Size = 2;
    public int BulletsPerShot = 1;
    public float ReloadTime = 0;
    public float clip;
    public float reloadtimer;
    public bool reloading = false;
    public int type;
    void Start()
    {
        clip = Size;
    }
    public virtual void SetHandler(MultiGun Given){
        Handler = Given;
    }
    // Update is called once per frame
    void Update()
    {
        if (reloading == true) {
            reloadtimer += Time.deltaTime;
            if (reloadtimer >= ReloadTime){
                clip = Size;
            }
        }
    }

    public virtual void ShootCheck(){
        if (clip >= BulletsPerShot){
            Handler.ShootConform(1,type);
            //print("We can Shoot.");
        }
    }
}
