using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiGun : MonoBehaviour
{
    public BarrelPoint Barrel;
    public MagPoint Mag;
    public GameObject BarrelP;
    public GameObject MagP;
    private GameObject barrel;
    private GameObject mag;
    // Start is called before the first frame update
    void Start()
    {
        UpdateComponentsVer2();
    }

    // Update is called once per frame
    void Update()
    {
        if //(Input.GetAxis("Fire1") == 1)
        (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
    }
    void UpdateComponentsVer2() {
        GameObject barrel = GameObject.FindWithTag("Barrel");
        if (barrel != null)
        {
            Barrel = barrel.GetComponent<BarrelPoint>();
            BarrelP.SetActive(false);
        }
        else {
            Barrel = BarrelP.GetComponent<BarrelPoint>();
        }
        GameObject mag = GameObject.FindWithTag("Mag");
        if (mag != null)
        {
            Mag = mag.GetComponent<MagPoint>();
            MagP.SetActive(false);
        }
        else {
            Mag = MagP.GetComponent<MagPoint>();
        }
        Mag.SetHandler(this);
    }
    void Shoot(){
        Mag.ShootCheck();
        //print("Can we shoot?");
    }
    public void ShootConform(float damage, int type){
        Barrel.ShootConfirm(damage,type);
        //print("Fire away!");
    }
}
