using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelPoint : MonoBehaviour
{
    public GameObject last_bullet;
    public int BULLETSPEED = 40000;
    public GameObject TERRAIN;
    public Vector3 dir;
    public GameObject Spawn;
    public GameObject Bullet;
    // Start is called before the first frame update
    void Start()
    {
        TERRAIN = GameObject.FindWithTag("Terrain");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public virtual void ShootConfirm(float damage, int type)
    {
        dir = Spawn.transform.position - this.transform.position;
        last_bullet = Instantiate(Bullet,Spawn.transform.position,Spawn.transform.rotation,TERRAIN.transform);
        last_bullet.GetComponent<Rigidbody>().AddForce(dir*BULLETSPEED);
        last_bullet.GetComponent<Bullet>().damage *= damage;
        if (type == 1)
        {
            last_bullet.transform.localScale = last_bullet.transform.localScale * damage;
        }
        //print("Works");   
        //print(damage);
    }
}
