using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{   
    public float damage;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(this, 5.0f);
        print("Bullet Damage");
        print(damage);
    }
    private void OnCollisionEnter(Collision collision)
    {
        Destroy(this);
        if (collision.gameObject.CompareTag("Enemy")){ 
            Destroy(collision.gameObject.transform.parent.gameObject);
        }
    }
    
}
