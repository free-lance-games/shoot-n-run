using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    public int NumberOfEnemies;
    private int enemies_remaining;
    private GameObject[] Spawners;
    private float random_spawner;
    private float random_enemy;
    public GameObject[] Mobs;
    private float spawn_timer;
    public float SpawnTimer;
    void Start()
    {
        print("Start");
        enemies_remaining = NumberOfEnemies;
        Spawners = GameObject.FindGameObjectsWithTag("Spawner");
        if (Spawners.Length <= 0){
            gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Spawners = GameObject.FindGameObjectsWithTag("Spawner");
        spawn_timer += Time.deltaTime;
        if (spawn_timer > SpawnTimer){
            if (enemies_remaining > 0){
                random_spawner = UnityEngine.Random.Range(0,Spawners.Length);
                random_enemy = UnityEngine.Random.Range(0,Mobs.Length);
                Instantiate(Mobs[Convert.ToInt32(random_enemy)],Spawners[Convert.ToInt32(random_spawner)].transform);
                //print(Spawners[Convert.ToInt32(random_spawner)]);
                spawn_timer = 0;
                enemies_remaining -= 1;
            }
        }
    }
}
