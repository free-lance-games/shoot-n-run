using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Char1 : MonoBehaviour
{
    public float MouseSensX = 1;
    public float MouseSensY = 1;
    public Transform Player;
    public Transform Cam;
    private float Rot_x;
    private float Rot_y;
    private float FBMovement;
    private float LRMovement;
    public float speed = 10;
    private Vector3 Dir;
    private CharacterController controller;
    
    // Start is called before the first frame update
    void Start()
    {
        controller = Player.GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update(){
        float Mouse_X = Input.GetAxisRaw("Mouse X") * MouseSensX;
        float Mouse_Y = Input.GetAxisRaw("Mouse Y") * MouseSensY;
        Rot_x += Mouse_X;
        Rot_y -= Mouse_Y;
        Rot_y = Mathf.Clamp(Rot_y, -90.0f, 90.0f);
        Player.rotation = Quaternion.Euler(0, Rot_x, 0);
        Cam.rotation = Quaternion.Euler(Rot_y, Rot_x, 0);
    }
    void FixedUpdate(){
        float FBMovement = Input.GetAxis("Vertical");
        float LRMovement = Input.GetAxis("Horizontal");
        Dir = Player.right * LRMovement + Player.forward * FBMovement;
        controller.SimpleMove(Dir * speed);
    }
}