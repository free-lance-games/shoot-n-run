using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public int Size;
    private int clip;
    public int BulletsPerShot;
    public float Rechamber;
    private bool ready;
    private float rechambertimer;
    public float ReloadTime;
    private bool reloading;
    private float reloadtimer;
    // Start is called before the first frame update
    void Start()
    {
        clip = Size;
        ready = true;
    }

    // Update is called once per frame
    void Update()
    {
        //print(Time.deltaTime);
        //print(reloading);
        if (ready == false){
            //print("Rechambering...");
            rechambertimer += Time.deltaTime;
            if (rechambertimer >= Rechamber){
                ready = true;
                rechambertimer = 0;
            }
        }
        if (reloading == true){
            //print("Reloading...");
            ready = false;
            reloadtimer += Time.deltaTime;
            if (reloadtimer >= ReloadTime){
                clip = Size;
                reloadtimer = 0;
                reloading = false;
                ready = true;
            }
        }
        if (Input.GetAxis("Fire1") == 1) {
            Shoot();
        }
    }
    void Shoot(){
        if (clip >= BulletsPerShot){
            if (ready == true){
                print("BOOM!");
                clip -= BulletsPerShot;
                ready = false;
            }
        } else {
            reloading = true;
            print("Click...");
        }
    }
}
