using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleEnergyMag : MagPoint
{
    private float bulletcost;
    void Start()
    {
        clip = 100.0f;
    }
    // Size wyznacza maks. ilość pocisków
    // BulletsPerShot to ilość pocisków zjadanych na jedną salwę
    // ReloadTime time needed to fully recharge
    public override void SetHandler(MultiGun Given){
        Handler = Given;
    }
    void Update()
    {
        if (reloading == true){
            if (ReloadTime > reloadtimer){
                reloadtimer += Time.deltaTime;
            } else {
                reloading = false;
                reloadtimer = 0;
            }
        }
        if (clip < 100.0f && reloading == false){
            clip += 100.0f / 3 * Time.deltaTime;
            clip = Mathf.Clamp(clip, 0.0f, 100.0f);
        }
    }
    public override void ShootCheck(){
        if (clip < 100.0f/Size*BulletsPerShot){
            //figure out procentage of damage
            float dmg = (clip/(100/Size*BulletsPerShot));
            dmg = Mathf.Clamp(dmg, 0.0f, 1.0f);
            clip -= (100.0f/Size*BulletsPerShot);
            Handler.ShootConform(dmg,type);
            //print("We can Shoot.");
            reloading = true;
            reloadtimer = 0;
        } else if (clip > 100.0f/Size*BulletsPerShot) {
            Handler.ShootConform(1,type);
            clip -= (100.0f/Size*BulletsPerShot);
            //print("We can Shoot.");
            reloading = true;
            reloadtimer = 0;
        }
        clip = Mathf.Clamp(clip, 0.0f, 100.0f);
    }
}
