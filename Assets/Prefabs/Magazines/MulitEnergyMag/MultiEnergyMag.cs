using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiEnergyMag : MagPoint
{
    public float[] clips;
    private int current_clip = 0;
    void Start()
    {
        clips = new float[Size];
        for (int i = 0; i < clips.Length; i++)
        {
            clips[i] = 100.0f;
        }
    }
    // Size wyznacza maks. ilość pocisków
    // BulletsPerShot to ilość pocisków zjadanych na jedną salwą
    // ReloadTime time needed to fully recharge
    public override void SetHandler(MultiGun Given)
    {
        Handler = Given;
    }
    void Update()
    {
        for (int i = 0; i < clips.Length; i++)
        {
            if (clips[i] < 100.0f)
            {
                clips[i] += 100.0f / 3 * Time.deltaTime;
                clips[i] = Mathf.Clamp(clips[i], 0.0f, 100.0f);
            }
        }
    }
    public override void ShootCheck()
    {
        print(clips);
        print(1/clips[current_clip]/100.0f);
        if (clips[current_clip] == 100.0f)
        {
            Handler.ShootConform(1, type);
            clips[current_clip] = 0.0f;
            //print("We can Shoot.");
        } else {
            Handler.ShootConform((1/clips[current_clip]/100.0f),type);
            clips[current_clip] = 0.0f;
        }
        current_clip += 1;
        if (current_clip == clips.Length){
            current_clip = 0;
        }
    }
}