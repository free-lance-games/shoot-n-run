using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StdMag : MagPoint
{
    // Start is called before the first frame update
    void Start()
    {
        clip = Size;
    }
    // Update is called once per frame
    void Update()
    {
        if (reloading == true) {
            reloadtimer += Time.deltaTime;
            if (reloadtimer >= ReloadTime){
                clip = Size;
                reloadtimer = 0;
                reloading = false;
            }
        }
    }
    public override void ShootCheck(){
        if (clip >= BulletsPerShot){
            Handler.ShootConform(1, type);
            clip -= BulletsPerShot;
            //print("We can Shoot.");
        } else {
            reloading = true;
        }
    }
}
