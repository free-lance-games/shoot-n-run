using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ShotgunBarrel : BarrelPoint
{
    public int Pellets;
    public Vector2 Offset;
    private float bulletsize = 0.25f;
    // Start is called before the first frame update
    void Start()
    {
        TERRAIN = GameObject.FindWithTag("Terrain");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void ShootConfirm(float damage, int type)
    {
        //print(damage);
        //print(type);
        for (int i = 0; i < Pellets; i++)
        {
            Vector3 offset = Vector3.zero;
            offset.x = Random.Range(-Offset.x, Offset.x);
            offset.y = Random.Range(-Offset.y, Offset.y);
            offset.z = 0.5f;
            Spawn.transform.localPosition = offset;
            //print(Spawn.transform.position);
            dir = Spawn.transform.position - this.transform.position;
            last_bullet = Instantiate(Bullet, Spawn.transform.position, Spawn.transform.rotation, TERRAIN.transform);
            last_bullet.GetComponent<Rigidbody>().AddForce(dir * BULLETSPEED);
            last_bullet.GetComponent<Bullet>().damage *= damage;
            if (type == 1)
            {
                last_bullet.transform.localScale *= damage;
            }
            last_bullet.transform.localScale *= bulletsize;
        }
    }
}
